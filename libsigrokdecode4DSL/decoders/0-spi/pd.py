##
## This file is part of the libsigrokdecode project.
##
## Copyright (C) 2011 Gareth McMullin <gareth@blacksphere.co.nz>
## Copyright (C) 2012-2014 Uwe Hermann <uwe@hermann-uwe.de>
## Copyright (C) 2019 DreamSourceLab <support@dreamsourcelab.com>
## Copyright (C) 2020 Raptor Engineering, LLC <support@raptorengineering.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
##

import sigrokdecode as srd
from collections import namedtuple

Data = namedtuple('Data', ['ss', 'es', 'val'])

'''
OUTPUT_PYTHON format:

Packet:
[<ptype>, <data1>, <data2>]

<ptype>:
 - 'DATA': <data1> contains the MOSI data, <data2> contains the MISO data.
   The data is _usually_ 8 bits (but can also be fewer or more bits).
   Both data items are Python numbers (not strings), or None if the respective
   channel was not supplied.
 - 'BITS': <data1>/<data2> contain a list of bit values in this MOSI/MISO data
   item, and for each of those also their respective start-/endsample numbers.
 - 'CS-CHANGE': <data1> is the old CS# pin value, <data2> is the new value.
   Both data items are Python numbers (0/1), not strings. At the beginning of
   the decoding a packet is generated with <data1> = None and <data2> being the
   initial state of the CS# pin or None if the chip select pin is not supplied.
 - 'TRANSFER': <data1>/<data2> contain a list of Data() namedtuples for each
   byte transferred during this block of CS# asserted time. Each Data() has
   fields ss, es, and val.

Examples:
 ['CS-CHANGE', None, 1]
 ['CS-CHANGE', 1, 0]
 ['DATA', 0xff, 0x3a]
 ['BITS', [[1, 80, 82], [1, 83, 84], [1, 85, 86], [1, 87, 88],
           [1, 89, 90], [1, 91, 92], [1, 93, 94], [1, 95, 96]],
          [[0, 80, 82], [1, 83, 84], [0, 85, 86], [1, 87, 88],
           [1, 89, 90], [1, 91, 92], [0, 93, 94], [0, 95, 96]]]
 ['DATA', 0x65, 0x00]
 ['DATA', 0xa8, None]
 ['DATA', None, 0x55]
 ['CS-CHANGE', 0, 1]
 ['TRANSFER', [Data(ss=80, es=96, val=0xff), ...],
              [Data(ss=80, es=96, val=0x3a), ...]]
'''

# Key: (CPOL, CPHA). Value: SPI mode.
# Clock polarity (CPOL) = 0/1: Clock is low/high when inactive.
# Clock phase (CPHA) = 0/1: Data is valid on the leading/trailing clock edge.
spi_mode = {
    (0, 0): 0, # Mode 0
    (0, 1): 1, # Mode 1
    (1, 0): 2, # Mode 2
    (1, 1): 3, # Mode 3
}

class ChannelError(Exception):
    pass

class Decoder(srd.Decoder):
    api_version = 3
    id = '0:spi'
    name = '0:SPI'
    longname = 'Serial Peripheral Interface'
    desc = 'Full-duplex, synchronous, serial bus.'
    license = 'gplv2+'
    inputs = ['logic']
    outputs = ['spi']
    tags = ['Embedded/industrial']
    channels = (
        {'id': 'clk', 'type': 0, 'name': 'CLK', 'desc': 'Clock'},
    )
    optional_channels = (
        {'id': 'dq3', 'type': -1, 'name': 'DQ3', 'desc': 'DQ3'},
        {'id': 'dq2', 'type': -1, 'name': 'DQ2', 'desc': 'DQ2'},
        {'id': 'miso', 'type': 107, 'name': 'MISO', 'desc': 'Master in, slave out / DQ1'},
        {'id': 'mosi', 'type': 109, 'name': 'MOSI', 'desc': 'Master out, slave in / DQ0'},
        {'id': 'cs', 'type': -1, 'name': 'CS#', 'desc': 'Chip-select'},
    )
    options = (
        {'id': 'cs_polarity', 'desc': 'CS# polarity', 'default': 'active-low',
            'values': ('active-low', 'active-high')},
        {'id': 'cpol', 'desc': 'Clock polarity (CPOL)', 'default': 0,
            'values': (0, 1)},
        {'id': 'cpha', 'desc': 'Clock phase (CPHA)', 'default': 0,
            'values': (0, 1)},
        {'id': 'bitorder', 'desc': 'Bit order',
            'default': 'msb-first', 'values': ('msb-first', 'lsb-first')},
        {'id': 'wordsize', 'desc': 'Word size', 'default': 8},
    )
    annotations = (
        ('106', 'miso-data', 'MISO data'),
        ('108', 'mosi-data', 'MOSI data'),
        ('110', 'qspi-data', 'QSPI data'),
        ('112', 'qspi-byte', 'QSPI byte'),
    )
    annotation_rows = (
        ('miso-data', 'MISO data', (0,)),
        ('mosi-data', 'MOSI data', (1,)),
        ('qspi-data', 'QSPI data', (2,)),
        ('qspi-byte', 'QSPI byte', (3,)),
    )

    def __init__(self):
        self.reset()

    def reset(self):
        self.samplerate = None
        self.bitcount = 0
        self.nibblecount = 0
        self.bytecount_cs_qspi = 0
        self.qspidata = self.misodata = self.mosidata = 0
        self.misobits = []
        self.mosibits = []
        self.qspibits = []
        self.ss_block = -1
        self.qspi_ss_block = -1
        self.samplenum = -1
        self.ss_transfer = -1
        self.cs_was_deasserted = False
        self.have_cs = self.have_qspi = self.have_miso = self.have_mosi = None

    def start(self):
        self.out_ann = self.register(srd.OUTPUT_ANN)
        self.bw = (self.options['wordsize'] + 7) // 8

    def metadata(self, key, value):
       if key == srd.SRD_CONF_SAMPLERATE:
            self.samplerate = value

    def putw(self, data):
        self.put(self.ss_block, self.samplenum, self.out_ann, data)

    def putdata(self):
        # Pass MISO and MOSI bits and then data to the next PD up the stack.
        so = self.misodata if self.have_miso else None
        si = self.mosidata if self.have_mosi else None

        if self.have_miso:
            ss, es = self.misobits[-1][1], self.misobits[0][2]
        if self.have_mosi:
            ss, es = self.mosibits[-1][1], self.mosibits[0][2]

        # Dataword annotations.
        if self.have_miso:
            self.put(ss, es, self.out_ann, [0, ['%02X' % self.misodata]])
        if self.have_mosi:
            self.put(ss, es, self.out_ann, [1, ['%02X' % self.mosidata]])

    def putqspidata(self):
        if self.have_qspi:
            ss, es = self.qspibits[-1][1], self.qspibits[0][2]

        # Dataword annotations.
        if self.have_qspi:
            self.put(ss, es, self.out_ann, [2, ['%02X' % self.qspidata]])
            self.put(ss, es, self.out_ann, [3, ['%d' % self.bytecount_cs_qspi]])
            self.bytecount_cs_qspi += 1

    def reset_decoder_state(self):
        self.misodata = 0 if self.have_miso else None
        self.mosidata = 0 if self.have_mosi else None
        self.qspidata = 0 if self.have_qspi else None
        self.misobits = [] if self.have_miso else None
        self.mosibits = [] if self.have_mosi else None
        self.qspibits = [] if self.have_qspi else None
        self.bitcount = 0
        self.nibblecount = 0

    def reset_qspi_decoder_state(self):
        self.qspidata = 0 if self.have_qspi else None
        self.qspibits = [] if self.have_qspi else None
        self.nibblecount = 0

    def cs_asserted(self, cs):
        active_low = (self.options['cs_polarity'] == 'active-low')
        return (cs == 0) if active_low else (cs == 1)

    def handle_bit(self, dq3, dq2, miso, mosi, clk, cs):
        # If this is the first bit of a dataword, save its sample number.
        if self.bitcount == 0:
            self.nibblecount = 0;
            self.ss_block = self.samplenum
            self.cs_was_deasserted = \
                not self.cs_asserted(cs) if self.have_cs else False

        if self.nibblecount == 0:
            self.qspi_ss_block = self.samplenum

        ws = self.options['wordsize']
        bo = self.options['bitorder']

        # Receive MISO bit into our shift register.
        if self.have_miso:
            if bo == 'msb-first':
                self.misodata |= miso << (ws - 1 - self.bitcount)
            else:
                self.misodata |= miso << self.bitcount

        # Receive MOSI bit into our shift register.
        if self.have_mosi:
            if bo == 'msb-first':
                self.mosidata |= mosi << (ws - 1 - self.bitcount)
            else:
                self.mosidata |= mosi << self.bitcount

        if self.have_qspi:
            nibble = 0
            nibble |= dq3 << 3
            nibble |= dq2 << 2
            nibble |= miso << 1
            nibble |= mosi << 0
            self.qspidata = self.qspidata << 4
            self.qspidata |= nibble

        # Guesstimate the endsample for this bit (can be overridden below).
        es = self.samplenum
        if self.bitcount > 0:
            if self.have_miso:
                es += self.samplenum - self.misobits[0][1]
            elif self.have_mosi:
                es += self.samplenum - self.mosibits[0][1]

        if self.have_miso:
            self.misobits.insert(0, [miso, self.samplenum, es])
        if self.have_mosi:
            self.mosibits.insert(0, [mosi, self.samplenum, es])

        # Guesstimate the endsample for this word (can be overridden below).
        es = self.samplenum
        if self.nibblecount > 0:
            if self.have_qspi:
                es += self.samplenum - self.qspibits[0][1]

        if self.have_qspi:
            self.qspibits.insert(0, [dq3, self.samplenum, es])

        if self.bitcount > 0 and self.have_miso:
            self.misobits[1][2] = self.samplenum
        if self.bitcount > 0 and self.have_mosi:
            self.mosibits[1][2] = self.samplenum

        if self.nibblecount > 0 and self.have_qspi:
            self.qspibits[1][2] = self.samplenum

        self.bitcount += 1
        self.nibblecount += 1

        if self.nibblecount > 1:
            self.putqspidata()
            self.reset_qspi_decoder_state()

        # Continue to receive if not enough bits were received, yet.
        if self.bitcount != ws:
            return

        self.putdata()
        self.reset_decoder_state()

    def find_clk_edge(self, dq3, dq2, miso, mosi, clk, cs, first):
        if self.have_cs and (first or (self.matched & (0b1 << self.have_cs))):
            # Send all CS# pin value changes.
            oldcs = None if first else 1 - cs

            # Reset decoder state when CS# changes (and the CS# pin is used).
            self.bytecount_cs_qspi = 0
            self.reset_decoder_state()

        # We only care about samples if CS# is asserted.
        if self.have_cs and not self.cs_asserted(cs):
            return

        # Ignore sample if the clock pin hasn't changed.
        if first or not (self.matched & (0b1 << 0)):
            return

        # Found the correct clock edge, now get the SPI bit(s).
        self.handle_bit(dq3, dq2, miso, mosi, clk, cs)

    def decode(self):
        # The CLK input is mandatory. Other signals are (individually)
        # optional. Yet either MISO or MOSI (or both) must be provided.
        # Tell stacked decoders when we don't have a CS# signal.
        if not self.has_channel(0):
            raise ChannelError('CLK pin required.')
        self.have_qspi = self.has_channel(1) and self.has_channel(2)
        self.have_miso = self.has_channel(3)
        self.have_mosi = self.has_channel(4)
        if not self.have_miso and not self.have_mosi:
            raise ChannelError('Either MISO or MOSI (or both) pins required.')
        self.have_cs = self.has_channel(5)

        # We want all CLK changes. We want all CS changes if CS is used.
        # Map 'have_cs' from boolean to an integer index. This simplifies
        # evaluation in other locations.
        # Sample data on rising/falling clock edge (depends on mode).
        mode = spi_mode[self.options['cpol'], self.options['cpha']]
        if mode == 0 or mode == 3:   # Sample on rising clock edge
            wait_cond = [{0: 'r'}]
        else: # Sample on falling clock edge
            wait_cond = [{0: 'f'}]

        if self.have_cs:
            self.have_cs = len(wait_cond)
            wait_cond.append({5: 'e'})

        # "Pixel compatibility" with the v2 implementation. Grab and
        # process the very first sample before checking for edges. The
        # previous implementation did this by seeding old values with
        # None, which led to an immediate "change" in comparison.
        (clk, dq3, dq2, miso, mosi, cs) = self.wait({})
        self.find_clk_edge(dq3, dq2, miso, mosi, clk, cs, True)

        while True:
            (clk, dq3, dq2, miso, mosi, cs) = self.wait(wait_cond)
            self.find_clk_edge(dq3, dq2, miso, mosi, clk, cs, False)
